package kovand.hu.sketch3d.Vector;

import java.util.Arrays;

public class Vector2 extends Vector {

    public Vector2(float x,float y){
        super(Arrays.asList(new Float[]{x, y}));
    }
    public float getX(){
        return super.components.get(0);
    }
    public float getY(){
        return super.components.get(1);
    }
}
