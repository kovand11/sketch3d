package kovand.hu.sketch3d.Vector;

import java.util.Arrays;

public class Vector3 extends Vector {

    public Vector3(float x,float y,float z)
    {
        super(Arrays.asList(new Float[]{x, y, z}));
    }
    public Vector3(Vector v)
    {
        this(v.getComponent(0),v.getComponent(1),v.getComponent(2));
    }
    public float getX()
    {
        return super.components.get(0);
    }
    public float getY(){ return super.components.get(1); }
    public float getZ()
    {
        return super.components.get(2);
    }

}
