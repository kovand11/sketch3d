package kovand.hu.sketch3d.Vector;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Vector implements Serializable{
    List<Float> components;

    public Vector(List<Float> components) {
        this.components = components;

    }

    public int size()
    {
        return components.size();
    }

    public float getComponent(int i){
        return components.get(i);
    }

    @Override
    public String toString() {
        String str = "";
        str += "{";
        for (int i=0; i<components.size(); i++)
        {
            if (i != 0)
                str += ", ";
            str += components.get(i).toString();
        }
        str += "}";
        return str;
    }

    //static operations

    public static Vector add(Vector a,Vector  b)
    {
        List<Float> result = new ArrayList<Float>();
        if (a.size() != b.size()){
            throw new ArithmeticException("Vectors must have the same size.");
        }
        for (int i=0; i< a.size(); i++){
            result.add(a.getComponent(i)+b.getComponent(i));
        }
        return new Vector(result);
    }

    public static Vector multiply(Vector v,float s)
    {
        List<Float> result = new ArrayList<Float>();
        for (int i=0; i<v.size();i++){
            result.add(v.getComponent(i)*s);
        }
        return new Vector(result);
    }

    public static float length(Vector v)
    {
        float len = 0.0f;
        for (int i=0; i < v.size(); i++)
        {
            float c = v.getComponent(i);
            len += c*c;
        }
        return (float)Math.sqrt(len);
    }

    public static float dotProduct(Vector a,Vector  b){
        float result = 0;
        if (a.size() != b.size()){
            throw new ArithmeticException("Vectors must have the same size.");
        }
        for (int i=0; i< a.size(); i++){
            result += a.getComponent(i)*b.getComponent(i);
        }
        return result;
    }

    public static float distance(Vector a,Vector b)
    {
        Vector diff = add(a,multiply(b,-1.0f));
        return (float)Math.sqrt(dotProduct(diff,diff));
    }

    //Array vector support

    public static float[] addAsNorm(float[] a, float[] b)
    {
        float[] result = new float[4];
        result[0] = a[0]/a[3] + b[0]/b[3];
        result[1] = a[1]/a[3] + b[1]/b[3];
        result[2] = a[2]/a[3] + b[2]/b[3];
        result[3] = 1.0f;
        return result;
    }

    public static float[] subtractAsNorm(float[] a, float[] b)
    {
        float[] result = new float[4];
        result[0] = a[0]/a[3] - b[0]/b[3];
        result[1] = a[1]/a[3] - b[1]/b[3];
        result[2] = a[2]/a[3] - b[2]/b[3];
        result[3] = 1.0f;
        return result;
    }

    public static float[] multiplyAsNorm(float[] v, float s)
    {
        float[] result = new float[4];
        result[0] = v[0]/v[3]*s;
        result[1] = v[1]/v[3]*s;
        result[2] = v[2]/v[3]*s;
        result[3] = 1.0f;
        return result;
    }

    public static float lengthAsNorm(float[] v)
    {
        float sqSum = v[0]*v[0] + v[1]*v[1] + v[2]*v[2];
        return (float)Math.sqrt(sqSum)/v[3];
    }

    public static String toString(float[] v)
    {
        String str = "";
        str += "{";
        for (int i=0; i<v.length; i++)
        {
            if (i != 0)
                str += ", ";
            str += Float.toString(v[i]);
        }
        str += "}";
        return str;
    }






    //Normalized device coord support



    public static float distanceNormDevice(Vector2 v1,Vector2 v2,float xLenght,float yLength)
    {
        float dx = (v1.getX()-v2.getX())*xLenght;
        float dy = (v1.getY()-v2.getY())*yLength;
        return (float)(Math.sqrt(dx*dx+dy*dy));
    }







    public static int findClosestNormDevice(Vector2 v,List<Vector2> vectorList,float xLenght,float yLength)
    {
        float minDist = Float.MAX_VALUE;
        int minInd=-1;
        for (int i=0; i < vectorList.size(); i++)
        {
            float dist = distanceNormDevice(v, vectorList.get(i), xLenght, yLength);
            if (dist < minDist) {
                minDist = dist;
                minInd = i;
            }
        }
        return minInd;
    }


    public static Vector3 nullSpace(Vector3 v1,Vector3 v2)
    {
        float x1 = v1.getX();
        float y1 = v1.getY();
        float z1 = v1.getZ();
        float x2 = v2.getX();
        float y2 = v2.getY();
        float z2 = v2.getZ();

        float x = -((y2*z1 - y1*z2)/(-x2*y1 + x1*y2));
        float y =  -((x2*z1 - x1*z2)/( x2*y1 - x1*y2));
        float z = 1;
        float length = Vector.length(new Vector3(x,y,z));

        return new Vector3(x/length,y/length,z/length);

    }



}
