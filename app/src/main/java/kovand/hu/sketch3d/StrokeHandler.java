package kovand.hu.sketch3d;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import android.view.MotionEvent;
import android.view.View;

import kovand.hu.sketch3d.Vector.Vector2;


public class StrokeHandler {


    public static int MIN_POINT_COUNT = 20;


    List<Vector2> curve;
    onStrokeListener listener;

    public StrokeHandler() {
        curve = new ArrayList<Vector2>();
    }

    public boolean onTouchEvent(View v,MotionEvent event)
    {
        int action = event.getAction();
        Vector2 p = new Vector2(event.getX(),event.getY());
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                curve.add(p);
                if (listener!=null){
                    listener.onStrokeBegin(p);
                }
                break;
            case MotionEvent.ACTION_MOVE:
                curve.add(p);
                if (listener!=null){
                    listener.onStroke(curve);
                }

                break;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_OUTSIDE:
                curve.add(p);
                if (listener!=null){
                    listener.onStrokeEnd(curve,curve.size()>=MIN_POINT_COUNT);
                }
                curve = new ArrayList<Vector2>();
                break;

            default:
                break;
        }
        return true;
    }

    public void setOnStrokeListener(onStrokeListener l)
    {
        listener = l;
    }

    public interface onStrokeListener
    {
        boolean onStrokeBegin(Vector2 address);
        boolean onStroke(List<Vector2> stroke);
        void onStrokeEnd(List<Vector2> stroke,boolean valid);
    }



}