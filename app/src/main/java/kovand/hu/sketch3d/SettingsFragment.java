package kovand.hu.sketch3d;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.Arrays;

import kovand.hu.sketch3d.Graphics.RenderingPreferences;

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener{

    String textPreferenceKeys[] = {"pref_background_color","pref_point_size","pref_point_color_active","pref_point_color_passive","pref_curve_size","pref_curve_color_active","pref_curve_color_passive",
            "pref_surface_size","pref_surface_color","pref_surface_grid_width","pref_surface_grid_height"};

    SharedPreferences sharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);


        for (String s : textPreferenceKeys)
        {
            EditTextPreference pref = (EditTextPreference)findPreference(s);
            pref.setSummary(pref.getText());
        }

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        Preference pref = findPreference(key);

        if (pref instanceof EditTextPreference) {
            EditTextPreference textPref = (EditTextPreference) pref;
            pref.setSummary(textPref.getText());
        }




        if (Arrays.asList(textPreferenceKeys).contains(key))
        {
            RenderingPreferences.refreshPreferences(getActivity());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity)activity).onSectionAttached(4);
    }

    public static SettingsFragment newInstance(){
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }
}
