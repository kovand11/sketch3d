package kovand.hu.sketch3d;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.PersistableBundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import kovand.hu.sketch3d.Graphics.RenderingPreferences;
import kovand.hu.sketch3d.Model.ModelContext;
import kovand.hu.sketch3d.Model.ModelSurface;
import kovand.hu.sketch3d.Model.ModelSurfacePlane;
import kovand.hu.sketch3d.Vector.Vector2;
import kovand.hu.sketch3d.Vector.Vector3;


/*
Data to hold on intentional shutdown:




Data to hold on forced quit:

- the current model
- the list of saved models



 */


public class MainActivity extends ActionBarActivity  implements NavigationDrawerFragment.NavigationDrawerCallbacks, SaveLoadDeleteListener {

    public static final String TAG = "MainActivity";

    ToggleButton panRotSwitch;

    //State data

    ModelContext modelContext = null;

    List<String> filesString;

    SketchBoardFragment sketchBoardFragment;

    public ModelContext getModelContext()
    {
        return modelContext;
    }

    public List<String> getFiles()
    {
        File[] files = getFilesDir().listFiles();
        filesString.clear();
        for (File f : files)
        {
            filesString.add(f.getName());
        }
        return filesString;
    }




    private NavigationDrawerFragment mNavigationDrawerFragment;
    private CharSequence mTitle;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate()");

        filesString = new ArrayList<String>();

        if (savedInstanceState != null) {
            modelContext = (ModelContext) savedInstanceState.getSerializable("modelContext");

        }

        if (modelContext == null)
        {
            modelContext = new ModelContext();
            ModelSurface testSurface = new ModelSurfacePlane(modelContext, new Vector3(0, 0, 0), new Vector3(1, 0, 0), new Vector3(0, 1, 0),
                    new ArrayList<UUID>(),new ArrayList<UUID>(),new ArrayList<Vector2>());
            modelContext.addElement(testSurface);
            modelContext.setActiveSurface(testSurface.getId());
        }




        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        RenderingPreferences.refreshPreferences(this);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState()");
        super.onSaveInstanceState(outState);
        outState.putSerializable("modelContext", modelContext);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {

        FragmentManager fragmentManager = getFragmentManager();

        if (position == 0){
            sketchBoardFragment = SketchBoardFragment.newInstance();
            fragmentManager.beginTransaction().
                    replace(R.id.container, sketchBoardFragment)
                    .commit();
        }
        else if (position == 1){
            fragmentManager.beginTransaction().
                    replace(R.id.container, DetailsFragment.newInstance())
                    .commit();
        }
        else if (position == 2){
            fragmentManager.beginTransaction().
                    replace(R.id.container, SaveLoadFragment.newInstance())
                    .commit();



        }
        else if (position == 3) {
            fragmentManager.beginTransaction().
                    replace(R.id.container, SettingsFragment.newInstance())
                    .commit();
        }

    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
            case 4:
                mTitle = getString(R.string.title_section4);
                break;

        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen())
        {
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();


            MenuItem item = menu.findItem(R.id.action_rotation_switch);
            View v = item.getActionView();
            panRotSwitch = (ToggleButton)v.findViewById(R.id.rotationLockSwitch);


            panRotSwitch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sketchBoardFragment.lockRotation(panRotSwitch.isChecked());
                }
            });

            return true;


        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        switch (id)
        {
            case R.id.action_edit:
                sketchBoardFragment.newSurface();
                break;
            case R.id.action_remove:
                sketchBoardFragment.deleteSelectedElements();
                break;

            case R.id.action_paste:
                sketchBoardFragment.makeCoons();
                break;



        }




        if (id == R.id.action_quit)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {


    }

    @Override
    public void onSave(String name) {
        Toast.makeText(this,"save:" + name,Toast.LENGTH_SHORT).show();

        FileOutputStream fileOutputStream;
        ObjectOutputStream objectOutputStream;

        try{
            fileOutputStream = openFileOutput(name, Context.MODE_PRIVATE);
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(modelContext);
            objectOutputStream.close();
            fileOutputStream.close();


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    @Override
    public void onLoad(String name) {
        Toast.makeText(this,"load:" + name,Toast.LENGTH_SHORT).show();

        FileInputStream fis;
        ObjectInputStream is;


        try{
            fis = openFileInput(name);
            is = new ObjectInputStream(fis);
            modelContext = (ModelContext) is.readObject();
            is.close();
            fis.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onDelete(String name) {
        deleteFile(name);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG," onDestroy()");

    }
}


