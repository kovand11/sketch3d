package kovand.hu.sketch3d;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.UUID;

import kovand.hu.sketch3d.Model.ModelContext;
import kovand.hu.sketch3d.Model.ModelElement;

public class DetailsFragment extends Fragment {

    public DetailsFragment(){}

    public static DetailsFragment newInstance(){
        DetailsFragment fragment = new DetailsFragment();
        return fragment;
    }

    ModelContext modelContext;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_details,container,false);



        ModelElement[] elems = new ModelElement[modelContext.getElementList().size()];

        for (int i=0; i < modelContext.getElementList().size(); i++)
        {
            elems[i] = modelContext.getElementList().get(i);
        }

        ListAdapter adapter = new MyAdapter(getActivity(),elems);

        ListView listView = (ListView)rootView.findViewById(R.id.detailsList);

        listView.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity)activity).onSectionAttached(2);

        modelContext = ((MainActivity)activity).getModelContext();
    }
}
