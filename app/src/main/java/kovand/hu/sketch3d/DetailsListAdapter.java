package kovand.hu.sketch3d;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import kovand.hu.sketch3d.Model.ModelElement;

class MyAdapter extends ArrayAdapter<ModelElement> {

    public MyAdapter(Context context, ModelElement[] values) {
        super(context, R.layout.layout_details_row, values);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater theInflater = LayoutInflater.from(getContext());
        View theView = theInflater.inflate(R.layout.layout_details_row, parent, false);
        ModelElement elem = getItem(position);
        TextView theTextView = (TextView) theView.findViewById(R.id.details_text);
        theTextView.setText(elem.getDescription());
        ImageView theImageView = (ImageView) theView.findViewById(R.id.details_image);

        if (elem.getType() == ModelElement.TYPE_POINT) {
            theImageView.setImageResource(R.drawable.point);
        }
        else if (elem.getType() == ModelElement.TYPE_CURVE){
            theImageView.setImageResource(R.drawable.curve);
        }
        else if(elem.getType() == ModelElement.TYPE_SURFACE){
            theImageView.setImageResource(R.drawable.surface);
        }
        return theView;
    }
}

