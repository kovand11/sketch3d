package kovand.hu.sketch3d.Model;

import android.util.Log;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import kovand.hu.sketch3d.Graphics.PlaneSurface;
import kovand.hu.sketch3d.Graphics.RenderingPreferences;
import kovand.hu.sketch3d.Utility;
import kovand.hu.sketch3d.Vector.Vector;
import kovand.hu.sketch3d.Vector.Vector2;
import kovand.hu.sketch3d.Vector.Vector3;


public class ModelContext implements Serializable {

    List<ModelElement> elementList; //holds all model element
    UUID activeSurface;
    List<UUID> selectedElements;
    transient PlaneSurface workingSurfaceToRender;

    public ModelContext()
    {
        elementList = new ArrayList<ModelElement>();
        selectedElements = new ArrayList<UUID>();
    }

    public void setActiveSurface(UUID id)
    {
        activeSurface = id;
        updateWorkingSurface();
    }

    public UUID getActiveSurface()
    {
        return activeSurface;
    }

    public List<ModelElement> getElementList()
    {
        return elementList;
    }

    public void deleteElement(UUID id)
    {
        selectedElements.remove(id);
        elementList.remove(getElement(id));
    }

    public void deleteElement(List<UUID> ids)
    {
        List<UUID> idsClone = new ArrayList<>(ids);
        for (UUID id : idsClone)
        {
            deleteElement(id);
        }

    }



    public boolean isSelected(UUID id)
    {
        for (UUID i : selectedElements)
        {
            if (i.equals(id))
                return true;
        }

        return false;
    }

    public boolean select(UUID id)
    {
        boolean exists = getElement(id) != null;
        if (exists)
        {
            if (!selectedElements.contains(id)) {
                selectedElements.add(id);
                getElement(id).setActive(true);
                return true;
            }
        }
        return exists;

    }

    public boolean unSelect(UUID id)
    {
        boolean exists = getElement(id) != null;
        if (exists)
        {
            if (selectedElements.contains(id)) {
                selectedElements.remove(id);
                getElement(id).setActive(false);
                return true;
            }
        }
        return false;
    }

    public ModelElement getElement(UUID id)
    {
        ModelElement element = null;
        for (ModelElement e : elementList){
            if (e.getId().equals(id) ){
                element = e;
                break;
            }
        }
        return element;
    }

    public void addElement(ModelElement element)
    {
        elementList.add(element);
        updateWorkingSurface();
    }

    public void render(float[] mvpMatrix) {
        for (ModelElement e : elementList)
        {
            e.render(mvpMatrix);
        }
        workingSurfaceToRender.render(mvpMatrix, RenderingPreferences.SurfaceColor);
    }

    public UUID getElementByScreenPosition(Vector2 absolutePos,float xLength,float yLength,float[] mvp)
    {
        List<ModelPoint> pointList = new ArrayList<>();
        for (ModelElement elem : elementList)
        {
            if (elem.getType() == ModelElement.TYPE_POINT){
                pointList.add((ModelPoint)elem);
            }
        }




        List<Vector2> screenPointList = new ArrayList<Vector2>();
        for (ModelPoint p : pointList){
            ModelSurface surf = (ModelSurface)getElement(p.getParent());
            Vector3 eval = surf.decodeAddress(p.address);
            screenPointList.add(Utility.mapToScreenNorm(eval, mvp));
        }

        boolean isPointSelected = false;
        int closest = Vector.findClosestNormDevice(absolutePos,screenPointList,xLength,yLength);


        if (closest>=0){
            float dist = Vector.distanceNormDevice(screenPointList.get(closest), absolutePos,xLength,yLength);
            if (dist<25.0f)
            {
                return pointList.get(closest).getId();
            }
        }



        List<ModelCurve> curveList = new ArrayList<>();
        for (ModelElement elem : elementList)
        {
            if (elem.getType() == ModelElement.TYPE_CURVE){
                curveList.add((ModelCurve)elem);
            }
        }

        List<Float> distanceList = new ArrayList<>();
        for (ModelCurve c : curveList)
        {
            ModelSurface surf = (ModelSurface)getElement(c.getParent());
            List<Vector3> decoded = surf.decodeAddress(c.getAddressList());
            List<Vector2> screenNorm = Utility.mapToScreenNorm(decoded, mvp);
            int closestIndex = Vector.findClosestNormDevice(absolutePos, screenNorm, xLength, yLength);
            float dist = Vector.distanceNormDevice(screenNorm.get(closestIndex), absolutePos, xLength, yLength);
            distanceList.add(dist);
        }

        float minDist = Float.MAX_VALUE;
        int minInd = -1;
        for (int i = 0; i < distanceList.size(); i++) {
            float d = distanceList.get(i);
            if (d < minDist) {
                minDist = d;
                minInd = i;
            }

        }
        if (minDist<30.0f)
        {
            return curveList.get(minInd).getId();
        }



        return null;
    }


    //Element generators
    //

    //two points who share the same parent
    public ModelSurfacePlane generatePerpedicularSurface(ModelPoint p1,ModelPoint p2)
    {
        if (!p1.getParent().equals(p2.getParent()))
        {
            return null;
        }

        ModelSurfacePlane surf = (ModelSurfacePlane)getElement(p1.getParent());

        Vector3 p1Addr = surf.decodeAddress(p1.address);
        Vector3 p2Addr = surf.decodeAddress(p2.address);

        Vector3 pole = p1Addr;
        Vector3 p1p2diff = new Vector3(Vector.add(p2Addr, Vector.multiply(p1Addr, -1)));
        Vector3 v1 = new Vector3(Vector3.multiply(p1p2diff, 1 / Vector.length(p1p2diff)));
        Vector3 v2 = Vector3.nullSpace(surf.getX(), surf.getY());

        List<UUID> dependencies = new ArrayList<>();
        dependencies.add(surf.getId());
        dependencies.add(p1.getId());
        dependencies.add(p2.getId());

        List<UUID> extraPoints = new ArrayList<>();
        extraPoints.add(p1.getId());
        extraPoints.add(p2.getId());

        List<Vector2> extraPointsAddresses = new ArrayList<>();
        extraPointsAddresses.add(new Vector2(0.0f,0.0f));
        extraPointsAddresses.add(new Vector2(Vector.length(p1p2diff),0.0f));

        ModelSurfacePlane newSurf = new ModelSurfacePlane(this,pole,v1,v2,dependencies,extraPoints,extraPointsAddresses);

        return newSurf;

    }

    public List<UUID> getSelectedElements()
    {
        return selectedElements;
    }



    public void updateWorkingSurface() {
        ModelSurface asurf = (ModelSurface) getElement(activeSurface);
        if (asurf == null)
            return;
        float minX = Float.MAX_VALUE;
        float maxX = -Float.MAX_VALUE;
        float minY = Float.MAX_VALUE;
        float maxY = -Float.MAX_VALUE;
        boolean isAnyPoint = false;

        for (ModelElement elem : elementList) {
            if (elem.getType() == ModelElement.TYPE_POINT) {
                if (((ModelPoint) elem).getParent() == activeSurface) {
                    isAnyPoint = true;
                    Vector2 addr = ((ModelPoint) elem).address;
                    if (minX>addr.getX()) minX = addr.getX();
                    if (maxX<addr.getX()) maxX = addr.getX();
                    if (minY>addr.getY()) minY = addr.getY();
                    if (maxY<addr.getY()) maxY = addr.getY();
                }

                //same for curves
            }

        }
        for (Vector2 addr : asurf.extraPointAddresses)
        {
            //same for extra points
        }

        Log.d("minX",Float.toString(minX));
        Log.d("maxX",Float.toString(maxX));
        Log.d("minY",Float.toString(minY));
        Log.d("maxY",Float.toString(maxY));


        //TODO

        ModelSurfacePlane msp = (ModelSurfacePlane)asurf;

        float width = maxX-minX;
        float height = maxY-minY;

        if (width < 300.0f){
            width = 300.0f;
        }

        if (height < 300.0f){
            height = 300.0f;
        }

        Vector3 pole = asurf.decodeAddress(new Vector2(minX - 0.1f*width, minY - 0.1f*height));
        Vector3 xVec = new Vector3(Vector.multiply(msp.getX(),1.2f*(maxX-minX)));
        Vector3 yVec = new Vector3(Vector.multiply(msp.getY(),1.2f*(maxY-minY)));




        workingSurfaceToRender = new PlaneSurface(pole,xVec,yVec);


        int i = 0;

    }

    //Its a temp. impl. since it needs more strict check
    //TODO
    public void generateCoons()
    {
        //based on for selected curves
        //must be in correct order

        try{
            ModelCurve mc0 = (ModelCurve)getElement(selectedElements.get(0));
            ModelCurve mc1 = (ModelCurve)getElement(selectedElements.get(1));
            ModelCurve md0 = (ModelCurve)getElement(selectedElements.get(2));
            ModelCurve md1 = (ModelCurve)getElement(selectedElements.get(3));
            ModelSurface c0surf = (ModelSurface)getElement(mc0.getParent());
            List<Vector3> c0 = c0surf.decodeAddress(mc0.getAddressList());
            ModelSurface c1surf = (ModelSurface)getElement(mc1.getParent());
            List<Vector3> c1 = c1surf.decodeAddress(mc1.getAddressList());
            ModelSurface d0surf = (ModelSurface)getElement(md0.getParent());
            List<Vector3> d0 = d0surf.decodeAddress(md0.getAddressList());
            ModelSurface d1surf = (ModelSurface)getElement(md1.getParent());
            List<Vector3> d1 = d1surf.decodeAddress(md1.getAddressList());


            ModelSurfaceCoons coonsPatch = new ModelSurfaceCoons(this,c0,c1,d0,d1,null,null,null);

            this.addElement(coonsPatch);





        }
        catch (Exception e)
        {
            return;
        }




    }


    private void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        updateWorkingSurface();
    }


}
