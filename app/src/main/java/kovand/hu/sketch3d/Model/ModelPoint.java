package kovand.hu.sketch3d.Model;


import java.util.UUID;

import kovand.hu.sketch3d.Graphics.RenderingPreferences;
import kovand.hu.sketch3d.Graphics.Vector3Renderable;
import kovand.hu.sketch3d.Vector.Vector2;

public class ModelPoint extends ModelElement {

    public UUID getParent() {
        return parent;
    }

    UUID parent;
    Vector2 address; //parent relative
    Vector3Renderable renderablePoint;

    public ModelPoint(ModelContext context,UUID parent ,Vector2 address)
    {
        super(context,UUID.randomUUID());
        this.parent = parent;
        this.address = address;
        ModelSurface parentSurface = (ModelSurface)getContext().getElement(parent);
        renderablePoint = new Vector3Renderable(parentSurface.decodeAddress(address));
    }

    public ModelPoint(ModelPoint prototype, Vector2 newAddress)
    {
        super(prototype.getContext(), prototype.getId());
        parent = prototype.getParent();
        address = newAddress;
        ModelSurface parentSurface = (ModelSurface)getContext().getElement(parent);
        renderablePoint = new Vector3Renderable(parentSurface.decodeAddress(address));

    }



    @Override
    public int getType() {
        return ModelElement.TYPE_POINT;
    }

    @Override
    public int getSubType() {
        return ModelElement.SUBTYPE_POINT_NORMAL;
    }

    @Override
    public String getDescription() {
        return "uuid= " + getId().toString() + " parent=" + parent.toString()+ " address="+address.toString();
    }

    @Override
    public void render(float[] mvpMatrix) {
        if (active){
            renderablePoint.render(mvpMatrix, RenderingPreferences.PointColorActive);
        }
        else {
            renderablePoint.render(mvpMatrix, RenderingPreferences.PointColorPassive);
        }
    }

}
