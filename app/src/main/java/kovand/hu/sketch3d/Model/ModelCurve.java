package kovand.hu.sketch3d.Model;


import java.util.List;
import java.util.UUID;

import kovand.hu.sketch3d.Graphics.PolyLine;
import kovand.hu.sketch3d.Graphics.PolyLineRenderable;
import kovand.hu.sketch3d.Graphics.RenderingPreferences;
import kovand.hu.sketch3d.Graphics.Vector3Renderable;
import kovand.hu.sketch3d.Vector.Vector2;
import kovand.hu.sketch3d.Vector.Vector3;

public class ModelCurve extends ModelElement {



    public UUID getParent() {
        return parent;
    }

    UUID parent;

    public List<Vector2> getAddressList() {
        return addressList;
    }

    List<Vector2> addressList; //parent relative
    PolyLineRenderable renderableCurve;

    public ModelCurve(ModelContext context,UUID parent ,List<Vector2> addressList)
    {
        super(context,UUID.randomUUID());
        this.parent = parent;
        this.addressList = addressList;
        ModelSurface parentSurface = (ModelSurface)getContext().getElement(parent);


        PolyLine p = new PolyLine();
        for (Vector2 v : addressList)
        {
            p.add(parentSurface.decodeAddress(v));
        }

        renderableCurve = new PolyLineRenderable(p);
    }

    @Override
    public int getType() {
        return ModelElement.TYPE_CURVE;
    }

    @Override
    public int getSubType() {
        return ModelElement.SUBTYPE_CURVE_NORMAL;
    }

    @Override
    public String getDescription() {
        return "type=polyline uuid=" + getId().toString() + " parent="+parent.toString()+ " addr=" + addressList.get(0).toString() + "..." + (addressList.get(addressList.size()-1)).toString();
    }

    @Override
    public void render(float[] mvpMatrix) {

        if (active)
        {
            renderableCurve.render(mvpMatrix, RenderingPreferences.CurveColorActive);
        }
        else
        {
            renderableCurve.render(mvpMatrix, RenderingPreferences.CurveColorPassive);
        }

    }
}
