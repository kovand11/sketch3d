package kovand.hu.sketch3d.Model;


import java.io.Serializable;
import java.security.PublicKey;
import java.util.UUID;


abstract public class ModelElement implements Serializable {

    public static final int TYPE_POINT = 0;
    public static final int TYPE_CURVE = 1;
    public static final int TYPE_SURFACE = 2;

    public static final int SUBTYPE_POINT_NORMAL = 3;
    public static final int SUBTYPE_CURVE_NORMAL = 4;
    public static final int SUBTYPE_SURFACE_PLAIN = 5;
    public static final int SUBTYPE_SURFACE_COONS = 6;

    private UUID id;
    String name;
    private ModelContext context;
    boolean active;

    public ModelElement(ModelContext context,UUID id){
        this.context = context;
        this.id = id;
        active = false;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public boolean isActive()
    {
        return active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public ModelContext getContext()
    {
        return this.context;
    }

    public abstract int getType();
    public abstract int getSubType();
    public abstract String getDescription();

    public abstract void render(float[] mvpMatrix);



}
