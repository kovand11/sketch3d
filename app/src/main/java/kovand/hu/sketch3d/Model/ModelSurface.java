package kovand.hu.sketch3d.Model;

import android.graphics.AvoidXfermode;

import java.util.List;
import java.util.UUID;

import kovand.hu.sketch3d.Vector.Vector2;
import kovand.hu.sketch3d.Vector.Vector3;


abstract public class ModelSurface extends ModelElement {

    public static final int PERP_2_POINTS = 0;

    public List<Vector2> getExtraPointAddresses() {
        return extraPointAddresses;
    }

    List<UUID> dependencies;
    List<UUID> extraPoints;
    List<Vector2> extraPointAddresses;


    ModelSurface (ModelContext context,UUID id,List<UUID> dependencies,List<UUID> extraPoints,List<Vector2> extraPointAddresses)
    {
        super(context,id);
        this.dependencies = dependencies;
        this.extraPoints = extraPoints;
        this.extraPointAddresses = extraPointAddresses;

    }

    @Override
    public int getType() {
        return ModelElement.TYPE_SURFACE;
    }



    abstract public Vector3 decodeAddress(Vector2 address);
    abstract public List<Vector3> decodeAddress(List<Vector2> addresses);



}
