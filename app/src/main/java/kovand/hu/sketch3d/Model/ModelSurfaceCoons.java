package kovand.hu.sketch3d.Model;

import java.util.List;
import java.util.UUID;

import kovand.hu.sketch3d.Graphics.CoonsPatch;
import kovand.hu.sketch3d.Graphics.PolyLine;
import kovand.hu.sketch3d.Graphics.RenderingPreferences;
import kovand.hu.sketch3d.Vector.Vector;
import kovand.hu.sketch3d.Vector.Vector2;
import kovand.hu.sketch3d.Vector.Vector3;

public class ModelSurfaceCoons extends ModelSurface {



    CoonsPatch patch;


    ModelSurfaceCoons(ModelContext context,List<Vector3> c0,List<Vector3> c1,List<Vector3> d0,List<Vector3> d1,List<UUID> dependencies,List<UUID> extraPoints,List<Vector2> extraPointAddresses)
    {
        super(context,UUID.randomUUID(),dependencies,extraPoints,extraPointAddresses);
        patch = new CoonsPatch(new PolyLine(c0),new PolyLine(c1),new PolyLine(d0),new PolyLine(d1));
        patch.generateRenderingData();

    }


    @Override
    public Vector3 decodeAddress(Vector2 address) {
        return null;
    }

    @Override
    public List<Vector3> decodeAddress(List<Vector2> addresses) {
        return null;
    }

    @Override
    public int getSubType() {
        return ModelElement.SUBTYPE_SURFACE_COONS;
    }

    @Override
    public String getDescription() {
        return "type=Plane uuid="+getId().toString();
    }

    @Override
    public void render(float[] mvpMatrix) {
        patch.render(mvpMatrix, RenderingPreferences.SurfaceColor);
    }
}
