package kovand.hu.sketch3d.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import kovand.hu.sketch3d.Vector.Vector;
import kovand.hu.sketch3d.Vector.Vector2;
import kovand.hu.sketch3d.Vector.Vector3;

public class ModelSurfacePlane extends ModelSurface {

    public Vector3 getPole() {
        return pole;
    }

    public Vector3 getX() {
        return x;
    }

    public Vector3 getY() {
        return y;
    }

    Vector3 pole;
    Vector3 x;
    Vector3 y;



    public ModelSurfacePlane(ModelContext context,Vector3 pole,Vector3 x,Vector3 y,List<UUID> dependencies,List<UUID> extraPoints,List<Vector2> extraPointAddresses)
    {
        super(context, UUID.randomUUID(),dependencies,extraPoints,extraPointAddresses);
        this.pole = pole;
        this.x = x;
        this.y = y;
    }

    @Override
    public Vector3 decodeAddress(Vector2 address) {
        Vector offset = Vector3.add(Vector.multiply(x, address.getX()), Vector.multiply(y, address.getY()));
        return new Vector3(pole.getX() + offset.getComponent(0),pole.getY() + offset.getComponent(1),pole.getZ() + offset.getComponent(2));
    }

    @Override
    public List<Vector3> decodeAddress(List<Vector2> addresses) {
        List<Vector3> decoded = new ArrayList<>();
        for (Vector2 v : addresses)        {
            decoded.add(decodeAddress(v));
        }
        return decoded;
    }

    @Override
    public int getSubType() {
        return ModelElement.SUBTYPE_SURFACE_PLAIN;
    }

    @Override
    public String getDescription() {
        return  "type=Plane uuid="+getId().toString() + " pole="+pole.toString()+" x="+x.toString()+" y="+y.toString();
    }

    @Override
    public void render(float[] mvpMatrix) {

    }


}
