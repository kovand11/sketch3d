package kovand.hu.sketch3d;

import android.opengl.Matrix;

import java.util.ArrayList;
import java.util.List;

import kovand.hu.sketch3d.Vector.Vector;
import kovand.hu.sketch3d.Vector.Vector2;
import kovand.hu.sketch3d.Vector.Vector3;

public class Utility {

    public static Vector2 mapToScreenNorm(Vector3 modelPos,float [] mvp)
    {
        float[] modelPosNorm = {modelPos.getX(),modelPos.getY(),modelPos.getZ(),1.0f};
        float[] screen3D = new float[4];
        Matrix.multiplyMV(screen3D, 0, mvp, 0, modelPosNorm, 0);
        Vector2 screen = new Vector2(screen3D[0]/screen3D[3],screen3D[1]/screen3D[3]);
        return screen;
    }

    public static List<Vector2> mapToScreenNorm(List<Vector3> modelPosList,float [] mvp)
    {
        List<Vector2> mapped = new ArrayList<>();
        for (Vector3 v : modelPosList){
            mapped.add(mapToScreenNorm(v,mvp));
        }
        return mapped;
    }


    public static Vector2 mapToSurface(Vector2 screenPoint,Vector3 pole,Vector3 v1,Vector3 v2, float[] mvp)
    {
        float[] p1 = new float[4];
        p1[0] = screenPoint.getX();
        p1[1] = screenPoint.getY();
        p1[2] = 0.5f;
        p1[3] = 1.0f;


        float[] p2 = new float[4];
        p2[0] = screenPoint.getX();
        p2[1] = screenPoint.getY();
        p2[2] = -0.5f;
        p2[3] = 1.0f;



        float[] invmvp = new float[16];
        Matrix.invertM(invmvp, 0, mvp, 0);


        float[] p1tr = new float[4];
        Matrix.multiplyMV(p1tr, 0, invmvp, 0, p1, 0);
        float[] p2tr = new float[4];
        Matrix.multiplyMV(p2tr, 0, invmvp, 0, p2, 0);
        float[] p1trp2trdiff = Vector.subtractAsNorm(p2tr, p1tr);





        float[] mat = new float[16];
        mat[0] = v1.getX();
        mat[1] = v1.getY();
        mat[2] = v1.getZ();
        mat[3] = 0.0f;
        mat[4] = v2.getX();//
        mat[5] = v2.getY();
        mat[6] = v2.getZ();
        mat[7] = 0.0f;
        mat[8] = p1trp2trdiff[0]/p1trp2trdiff[3];//
        mat[9] = p1trp2trdiff[1]/p1trp2trdiff[3];
        mat[10] = p1trp2trdiff[2]/p1trp2trdiff[3];
        mat[11] = 0.0f;
        mat[12] = 0.0f;//
        mat[13] = 0.0f;
        mat[14] = 0.0f;
        mat[15] = 1.0f;

        float[] inv = new float[16];
        Matrix.invertM(inv, 0, mat, 0);


        float[] vec = new float[4];
        vec[0] = p1tr[0]/p1tr[3] - pole.getX();
        vec[1] = p1tr[1]/p1tr[3] - pole.getY();
        vec[2] = p1tr[2]/p1tr[3] - pole.getZ();
        vec[3] = 0.0f;

        float[] solution = new float[4];
        Matrix.multiplyMV(solution, 0, inv, 0, vec, 0);


        return new Vector2(solution[0], solution[1]);
    }


    public static List<Vector2> mapToSurface(List<Vector2> screenPointList,Vector3 pole,Vector3 v1,Vector3 v2, float[] mvp)
    {
        List<Vector2> mapped = new ArrayList<Vector2>();
        for (Vector2 v : screenPointList)
        {
            mapped.add(mapToSurface(v,pole,v1,v2,mvp));
        }
        return mapped;


    }


    public static Vector2 fromAndroidToNormalizedDevice(Vector2 v,int width,int height)
    {
        return new Vector2(v.getX()/width*2.0f-1.0f, 1.0f-v.getY()/height*2.0f);
    }

    public static List<Vector2> fromAndroidToNormalizedDevice(List<Vector2> points,int width,int height)
    {
        List<Vector2> mapped = new ArrayList<Vector2>();
        for (Vector2 v : points)
        {
            mapped.add(fromAndroidToNormalizedDevice(v,width,height));
        }
        return mapped;
    }



}
