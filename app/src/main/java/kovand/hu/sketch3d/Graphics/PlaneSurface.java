package kovand.hu.sketch3d.Graphics;

import java.io.Serializable;

import kovand.hu.sketch3d.Vector.Vector;
import kovand.hu.sketch3d.Vector.Vector3;



public class PlaneSurface extends ParametricSurface implements Serializable {

    Vector3 orig;
    Vector3 x;
    Vector3 y;

    public PlaneSurface(Vector3 orig,Vector3 x,Vector3 y)
    {
        this.orig = orig;
        this.x = x;
        this.y = y;
        generateRenderingData();
    }

    @Override
    public Vector evaluate(float u, float v) {
        Vector offset = Vector.add(Vector.multiply(x, u), Vector.multiply(y, v));
        return Vector.add(orig,offset);
    }
}
