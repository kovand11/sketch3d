package kovand.hu.sketch3d.Graphics;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class RenderingPreferences {
    public static float[] BackgroundColor;
    public static float PointSize;
    public static float[] PointColorActive;
    public static float[] PointColorPassive;
    public static float CurveSize;
    public static float[] CurveColorActive;
    public static float[] CurveColorPassive;
    public static float SurfaceSize;
    public static float[] SurfaceColor;
    public static int SurfaceWidth;
    public static int SurfaceHeight;



    public static void refreshPreferences(Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        BackgroundColor = getColorFromHex(sharedPreferences.getString("pref_background_color", "FFFFFF"));
        PointSize = Float.parseFloat(sharedPreferences.getString("pref_point_size", "20.0"));
        PointColorActive = getColorFromHex(sharedPreferences.getString("pref_point_color_active", "000000"));
        PointColorPassive = getColorFromHex(sharedPreferences.getString("pref_point_color_passive", "000000"));
        CurveSize = Float.parseFloat(sharedPreferences.getString("pref_curve_size", "5.0"));
        CurveColorActive = getColorFromHex(sharedPreferences.getString("pref_curve_color_active", "000000"));
        CurveColorPassive = getColorFromHex(sharedPreferences.getString("pref_curve_color_passive", "000000"));
        SurfaceSize = Float.parseFloat(sharedPreferences.getString("pref_surface_size", "2.0"));
        SurfaceColor = getColorFromHex(sharedPreferences.getString("pref_surface_color", "000000"));
        SurfaceWidth = Integer.parseInt(sharedPreferences.getString("pref_surface_grid_width", "50"));
        SurfaceHeight = Integer.parseInt(sharedPreferences.getString("pref_surface_grid_height", "50"));
    }

    public static float[] getColorFromHex(String s)
    {
        int ir = Integer.parseInt(s.substring(0,2),16);
        int ig = Integer.parseInt(s.substring(2,4),16);
        int ib = Integer.parseInt(s.substring(4,6),16);
        return new float[]{ir/255.0f,ig/255.0f,ib/255.0f,1.0f};
    }
}
