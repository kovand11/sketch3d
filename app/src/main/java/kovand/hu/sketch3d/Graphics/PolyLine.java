package kovand.hu.sketch3d.Graphics;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import kovand.hu.sketch3d.Vector.Vector;
import kovand.hu.sketch3d.Vector.Vector3;

public class PolyLine extends ParametricCurve {

    public PolyLine(List<Vector3> points)
    {
        this.points = points;
    }

    public PolyLine(PolyLine p)
    {
        this.points = p.getPoints();
    }

    public List<Vector3> getPoints() {
        return points;
    }


    List<Vector3> points;

    public PolyLine()
    {
        points = new ArrayList<Vector3>();
    }

    public void add(Vector3 v)
    {
        points.add(v);
    }

    public int size()
    {
        return points.size();
    }


    @Override
    public Vector evaluate(float t) {
        if (points.size() < 2){
            throw new ArithmeticException("PolyLine size must be at least 2");
        }
        int floor = (int)Math.floor(t * (points.size() - 1));
        int ceil = (int)Math.ceil(t*(points.size()-1));
        float frac = t*(points.size() - 1) - floor;
        Vector p1 = points.get(floor);
        Vector p2 = points.get(ceil);
        return Vector.add(Vector.multiply(p1,1.0f-frac),Vector.multiply(p2,frac));
    }
}
