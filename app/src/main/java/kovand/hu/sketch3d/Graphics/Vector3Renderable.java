package kovand.hu.sketch3d.Graphics;


import android.opengl.GLES20;

import java.io.IOException;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import kovand.hu.sketch3d.Vector.Vector3;

public class Vector3Renderable extends Vector3 implements Serializable{

    //Rendering data
    private static final int COORDS_PER_VERTEX = 3;
    private static final int BYTES_PER_FLOAT = 4;

    private static final String vertexShaderCode =
            "uniform float PointSize;" +
            "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 vPosition;" +
                    "void main() {" +
                    "  gl_Position = uMVPMatrix * vPosition;" +
                    "gl_PointSize = PointSize;" +
                    "}";

    private static final String fragmentShaderCode =
            "precision mediump float;" +
                    "uniform vec4 vColor;" +
                    "vec2 circCoord = 2.0 * gl_PointCoord - 1.0;"+
                    "void main() {" +
                    "if (dot(circCoord, circCoord) > 1.0) {" +
                    "   discard;" +
                    "}" +
                    "  gl_FragColor = vColor;" +
                    "}";

    transient private FloatBuffer vertexBuffer;

    private static int mProgram;
    private int mPositionHandle;
    private int mColorHandle;
    private int mSizeHandle;
    private int mMVPMatrixHandle;

    public Vector3Renderable(float x,float y,float z){
        super(x,y,z);
        generateData();
    }

    public  Vector3Renderable(Vector3 v)
    {
        super(v.getX(),v.getY(),v.getZ());
        generateData();
    }

    public void render(float[] mvpMatrix, float[] color) {
        GLES20.glUseProgram(mProgram);
        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");
        GLES20.glEnableVertexAttribArray(mPositionHandle);
        GLES20.glVertexAttribPointer(mPositionHandle, 3, GLES20.GL_FLOAT, true, 3 * BYTES_PER_FLOAT, vertexBuffer);
        mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
        GLES20.glUniform4fv(mColorHandle, 1, color, 0);
        mSizeHandle = GLES20.glGetUniformLocation(mProgram, "PointSize");
        GLES20.glUniform1f(mSizeHandle,RenderingPreferences.PointSize);
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);
        //GLES20.glDrawArrays(GLES20.GL_LINE_STRIP, 0, GRID_WIDTH*GRID_HEIGHT);
        GLES20.glDrawArrays(GLES20.GL_POINTS,0,1);
        GLES20.glDisableVertexAttribArray(mPositionHandle);

    }

    public void generateData()
    {
        //Prepare vertex data
        ByteBuffer bb = ByteBuffer.allocateDirect(BYTES_PER_FLOAT*COORDS_PER_VERTEX);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(getX());
        vertexBuffer.put(getY());
        vertexBuffer.put(getZ());
        vertexBuffer.position(0);
    }

    public static void generateShaders()
    {
        int vertexShader = GLRenderer.loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode);
        int fragmentShader = GLRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);
        mProgram = GLES20.glCreateProgram();
        GLES20.glAttachShader(mProgram, vertexShader);
        GLES20.glAttachShader(mProgram, fragmentShader);
        GLES20.glLinkProgram(mProgram);

    }


    private void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        generateData();
    }


    //TODO: try if the restoration of the shader handlers can be done too


}
