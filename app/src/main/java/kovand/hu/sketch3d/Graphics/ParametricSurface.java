package kovand.hu.sketch3d.Graphics;

import android.opengl.GLES20;
import android.util.Log;

import java.io.IOException;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;

import kovand.hu.sketch3d.Vector.Vector;

abstract public class ParametricSurface implements Serializable{
    public static final String TAG = "ParametricSurface";

    //Constants
    private static final short GRID_WIDTH = 50;
    private static final short GRID_HEIGHT = 50;
    private static final int COORDS_PER_VERTEX = 3;
    private static final int BYTES_PER_FLOAT = 4;
    private static final int BYTES_PER_SHORT = 2;


    //Rendering data

    private static final String vertexShaderCode =
            "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 vPosition;" +
                    "void main() {" +
                    "  gl_Position = uMVPMatrix * vPosition;" +
                    "}";

    private static final String fragmentShaderCode =
            "precision mediump float;" +
                    "uniform vec4 vColor;" +
                    "void main() {" +
                    "  gl_FragColor = vColor;" +
                    "}";

    transient private FloatBuffer vertexBuffer;
    transient private List<ShortBuffer> drawListBufferList;
    transient private List<Integer> drawListBufferSizeList;

    private static int mProgram;

    private int mPositionHandle;
    private int mColorHandle;
    private int mMVPMatrixHandle;




    public abstract Vector evaluate(float u,float v);

    public void render(float[] mvpMatrix, float[] color){

        GLES20.glUseProgram(mProgram);
        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");
        GLES20.glEnableVertexAttribArray(mPositionHandle);
        GLES20.glVertexAttribPointer(mPositionHandle, 3, GLES20.GL_FLOAT, true, 3 * BYTES_PER_FLOAT, vertexBuffer);
        mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
        GLES20.glUniform4fv(mColorHandle, 1, color, 0);
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);
        //GLES20.glDrawArrays(GLES20.GL_LINE_STRIP, 0, GRID_WIDTH*GRID_HEIGHT);
        for (int i=0;i<drawListBufferList.size();i++) {
            GLES20.glDrawElements(GLES20.GL_LINE_STRIP, drawListBufferSizeList.get(i), GLES20.GL_UNSIGNED_SHORT, drawListBufferList.get(i));
        }
        GLES20.glDisableVertexAttribArray(mPositionHandle);


    }

    public void generateRenderingData()
    {
        //Prepare vertex data
        ByteBuffer bb = ByteBuffer.allocateDirect(GRID_WIDTH * GRID_HEIGHT * BYTES_PER_FLOAT*COORDS_PER_VERTEX);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();

        for (int i=0; i<GRID_WIDTH; i++){
            for (int j=0; j<GRID_HEIGHT; j++) {
                Vector v = this.evaluate(i/(GRID_WIDTH-1.0f),j/(GRID_HEIGHT-1.0f));
                vertexBuffer.put(v.getComponent(0));
                vertexBuffer.put(v.getComponent(1));
                vertexBuffer.put(v.getComponent(2));
            }
        }
        vertexBuffer.position(0);

        //Prepare draw list
        List<List<Short>> drawOrderList = new ArrayList<List<Short>>();
        for (short i=0;i<GRID_WIDTH;i++)
        {
            List<Short> drawOrder = new ArrayList<Short>();
            for (short j=0;j<GRID_HEIGHT;j++){
                drawOrder.add((short) (i * GRID_HEIGHT + j));
            }
            drawOrderList.add(drawOrder);
        }
        for (short i=0;i<GRID_HEIGHT;i++)
        {
            List<Short> drawOrder = new ArrayList<Short>();
            for (short j=0;j<GRID_WIDTH;j++){
                drawOrder.add((short)(i+j*GRID_HEIGHT));
            }
            drawOrderList.add(drawOrder);
        }

        drawListBufferList = new ArrayList<ShortBuffer>();
        drawListBufferSizeList = new ArrayList<Integer>();


        for (int i=0;i<drawOrderList.size();i++)
        {
            ByteBuffer dlb = ByteBuffer.allocateDirect(drawOrderList.get(i).size() * BYTES_PER_SHORT);
            dlb.order(ByteOrder.nativeOrder());
            ShortBuffer drawListBuffer = dlb.asShortBuffer();
            for (int j=0;j<drawOrderList.get(i).size();j++){
                drawListBuffer.put(drawOrderList.get(i).get(j));
            }
            drawListBuffer.position(0);
            drawListBufferList.add(drawListBuffer);
            drawListBufferSizeList.add(drawOrderList.get(i).size());

        }
    }


    public static void generateShaders()
    {
        int vertexShader = GLRenderer.loadShader(GLES20.GL_VERTEX_SHADER,vertexShaderCode);
        int fragmentShader = GLRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);
        mProgram = GLES20.glCreateProgram();
        GLES20.glAttachShader(mProgram, vertexShader);
        GLES20.glAttachShader(mProgram, fragmentShader);
        GLES20.glLinkProgram(mProgram);

    }


}
