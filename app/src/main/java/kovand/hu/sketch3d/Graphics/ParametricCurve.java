package kovand.hu.sketch3d.Graphics;

import java.io.Serializable;

import kovand.hu.sketch3d.Vector.Vector;

abstract public class ParametricCurve implements Serializable {
    public abstract Vector evaluate(float t);
}
