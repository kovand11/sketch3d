package kovand.hu.sketch3d.Graphics;

import java.util.ArrayList;
import java.util.List;

import kovand.hu.sketch3d.Vector.Vector;
import kovand.hu.sketch3d.Vector.Vector3;

public class BSpline {
    public static final String TAG = "BSpline";

    protected List<Float> knots;
    protected List<Vector3> controlPoints;
    protected int p;
    protected int n;


    /** creates an empty bspline
     */
    public BSpline()
    {
        knots = new ArrayList<Float>();
        controlPoints =new ArrayList<Vector3>();
        p = 0;
        n = 0;

    }
    /** construct with all data defined
     */
    public BSpline(List<Float> knots,List<Vector3> controlPoints,int p,int n)
    {
        this.knots = knots;
        this.controlPoints = controlPoints;
        this.p = p;
        this.n = n;
    }

    /** approximates a given polyline, with a target error
     *
     * @param curve		the appriximated polyline
     * @param p_param	degree of the
     * @param n_param	n+1 contoll points
     */
    public void approximate(PolyLine curve,int p_param,int n_param)
    {
        p = p_param;
        n = n_param;

        List<Vector3> points = new ArrayList<Vector3>();
        List<Vector3> pointsGeneric = curve.getPoints();
        for (int i=0;i< pointsGeneric.size();i++){
            points.add((Vector3)pointsGeneric.get(i));
        }
        double [] px = new double[points.size()];
        double [] py = new double[points.size()];
        double [] pz = new double[points.size()];
        for (int i=0;i<points.size();i++)
        {
            px[i] = points.get(i).getX();
            py[i] = points.get(i).getY();
            pz[i] = points.get(i).getZ();
        }

        double[] result = NativeCurveLib.approximate(px, py, pz, p, n);

        //result must (n+p+2)*knots + (n+1)*controlPoints

        knots.clear();
        for (int i=0;i<n+p+2;i++)
        {
            knots.add((float)result[i]);

        }

        controlPoints.clear();
        for (int i=0;i<(n+1);i++)
        {
            Vector3 cp = new Vector3((float)result[n+p+2+3*i],
                    (float)result[n+p+2+3*i+1],
                    (float)result[n+p+2+3*i+2]);

            controlPoints.add(cp);

        }
    }


    /** Evaluates the b-spline with n points
     *
     * @param points number of points with equal u distance
     * @return the approximated polyline
     */
    public PolyLine evaluateN(int points)
    {

        double[] knots_arr = new double[knots.size()];
        for (int i=0;i<knots.size();i++)
        {
            knots_arr[i]=(double)knots.get(i);
        }

        double[] cpx = new double[controlPoints.size()];
        double[] cpy = new double[controlPoints.size()];
        double[] cpz = new double[controlPoints.size()];
        //
        for (int i=0;i<controlPoints.size();i++)
        {
            cpx[i] = (double)controlPoints.get(i).getX();
            cpy[i] = (double)controlPoints.get(i).getY();
            cpz[i] = (double)controlPoints.get(i).getZ();
        }

        double[] result = NativeCurveLib.evaluateN(knots_arr, cpx, cpy, cpz, points);

        PolyLine result_line = new PolyLine();

        for (int i=0;i<points;i++)
        {
            Vector3 point = new Vector3((float)result[3*i], (float)result[3*i+1], (float)result[3*i+2]);
            result_line.add(point);
        }

        return result_line;
    }



    public Vector3 evaluate(float u)
    {
        double[] knots_arr = new double[knots.size()];
        for (int i=0;i<knots.size();i++)
        {
            knots_arr[i]=(double)knots.get(i);
        }

        double[] cpx = new double[controlPoints.size()];
        double[] cpy = new double[controlPoints.size()];
        double[] cpz = new double[controlPoints.size()];
        //
        for (int i=0;i<controlPoints.size();i++)
        {
            cpx[i] = (double)controlPoints.get(i).getX();
            cpy[i] = (double)controlPoints.get(i).getY();
            cpz[i] = (double)controlPoints.get(i).getZ();
        }

        double[] result = NativeCurveLib.evaluate(knots_arr, cpx, cpy, cpz, u);

        return new Vector3((float)result[0],(float) result[1], (float)result[2]);
    }

    public ProjectionResult projectPoint(Vector3 p,int resolution,float dist_tol,float cos_tol)
    {
        double[] knots_arr = new double[knots.size()];
        for (int i=0;i<knots.size();i++)
        {
            knots_arr[i]=(double)knots.get(i);
        }

        double[] cpx = new double[controlPoints.size()];
        double[] cpy = new double[controlPoints.size()];
        double[] cpz = new double[controlPoints.size()];
        //
        for (int i=0;i<controlPoints.size();i++)
        {
            cpx[i] = (double)controlPoints.get(i).getX();
            cpy[i] = (double)controlPoints.get(i).getY();
            cpz[i] = (double)controlPoints.get(i).getZ();
        }

        double[] result = NativeCurveLib.projectPoint(knots_arr, cpx, cpy, cpz, p.getX(), p.getY(), p.getZ() , resolution, dist_tol, cos_tol);


        //TODO

        return null;
    }




    public List<Vector3> getControlPoints(){
        return controlPoints;
    }




    public List<Float> getKnots()
    {
        return knots;
    }

    public int getP()
    {
        return p;

    }

    public int getN()
    {
        return n;
    }

    public List<Vector3> evaluateAtKnots()
    {
        List<Vector3> list = new ArrayList<Vector3>();
        for (Float f : knots)
        {
            list.add(evaluate(f));
        }
        return list;
    }





    public class ProjectionResult
    {
        private float u;
        private Vector3 point;
        private float distance;

        public ProjectionResult(float u,Vector3 p,float d) {
            this.u = u;
            point = p;
            distance = d;
        }

        public float getU(){
            return u;
        }

        public Vector3 getPoint(){
            return point;
        }

        public float getDistance(){
            return distance;
        }

    }


}