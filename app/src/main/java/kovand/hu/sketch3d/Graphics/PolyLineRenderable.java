package kovand.hu.sketch3d.Graphics;

import android.opengl.GLES20;

import java.io.IOException;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.List;

import kovand.hu.sketch3d.Vector.Vector;

public class PolyLineRenderable extends PolyLine implements Serializable {
    //Constants
    private static final int COORDS_PER_VERTEX = 3;
    private static final int BYTES_PER_FLOAT = 4;


    //Rendering data
    private static final String vertexShaderCode =
            "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 vPosition;" +
                    "void main() {" +
                    "  gl_Position = uMVPMatrix * vPosition;" +
                    "}";

    private static final String fragmentShaderCode =
            "precision mediump float;" +
                    "uniform vec4 vColor;" +
                    "void main() {" +
                    "  gl_FragColor = vColor;" +
                    "}";

    transient private FloatBuffer vertexBuffer;
    transient private int vertexBufferSize;

    private static int mProgram;

    private int mPositionHandle;
    private int mColorHandle;
    private int mMVPMatrixHandle;


    public void render(float[] mvpMatrix, float[] color) {

        GLES20.glLineWidth(5.0f);


        GLES20.glUseProgram(mProgram);
        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");
        GLES20.glEnableVertexAttribArray(mPositionHandle);
        GLES20.glVertexAttribPointer(mPositionHandle, 3, GLES20.GL_FLOAT, true, 3 * BYTES_PER_FLOAT, vertexBuffer);
        mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
        GLES20.glUniform4fv(mColorHandle, 1, color, 0);
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);
        GLES20.glDrawArrays(GLES20.GL_LINE_STRIP, 0, vertexBufferSize);
        GLES20.glDisableVertexAttribArray(mPositionHandle);

        GLES20.glLineWidth(1.0f);


    }

    public PolyLineRenderable(PolyLine p){
        super(p);
        generateRenderingData();
    }


    void generateRenderingData()
    {
        //Prepare vertex data
        ByteBuffer bb = ByteBuffer.allocateDirect(BYTES_PER_FLOAT * COORDS_PER_VERTEX * this.size());
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        for (int i=0;i<this.size();i++)
        {
            Vector v = this.getPoints().get(i);

            vertexBuffer.put(v.getComponent(0));
            vertexBuffer.put(v.getComponent(1));
            vertexBuffer.put(v.getComponent(2));
        }
        vertexBuffer.position(0);
        vertexBufferSize = this.size();


    }

    public static void generateShaders()
    {
        int vertexShader = GLRenderer.loadShader(GLES20.GL_VERTEX_SHADER,vertexShaderCode);
        int fragmentShader = GLRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);
        mProgram = GLES20.glCreateProgram();
        GLES20.glAttachShader(mProgram, vertexShader);
        GLES20.glAttachShader(mProgram, fragmentShader);
        GLES20.glLinkProgram(mProgram);
    }

    private void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        generateRenderingData();
    }
}
