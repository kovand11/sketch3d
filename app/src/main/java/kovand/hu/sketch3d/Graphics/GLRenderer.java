package kovand.hu.sketch3d.Graphics;

import android.graphics.AvoidXfermode;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.Log;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import kovand.hu.sketch3d.Model.ModelContext;
import kovand.hu.sketch3d.Vector.Vector;
import kovand.hu.sketch3d.Vector.Vector3;


public class GLRenderer implements GLSurfaceView.Renderer {

    private static final float[] IDENTITY_MATRIX =    {1,0,0,0,
                                            0,1,0,0,
                                            0,0,1,0,
                                            0,0,0,1};

    //Style Sheet
    private static final float[] BACKGROUND_COLOR = {1.0f, 1.0f, 1.0f, 1.0f};
    private static final float[] POINT_COLOR = {0.184f, 0.310f, 0.310f,1.0f};
    private static final float[] SURFACE_COLOR = {0.753f, 0.753f, 0.753f, 1.0f};

    //Constants
    public static final float MODEL_FRONT_Z = 1.0f;
    public static final float MODEL_BACK_Z = 10000.0f;

    public static final float ROTATION_COEFF = 0.1f;

    private final float[] MVPMatrix = new float[16]; //viewMatrix*projectionMatrix
    private final float[] viewMatrix = new float[16]; //depends(eyeVec,centerVec,upVec)
    private final float[] projectionMatrix= new float[16]; //depends(aspect, fovy, zNear, zFar)


    //perspective
    float fovy;
    float aspect;
    float zNear;
    float zFar;

    //view


    float[] eyeVec;
    float[] centerVec;
    float[] upVec;
    float[] rightVec; //derived

    private CoonsPatch coonsPatch;
    private Vector3Renderable vec;

    private PolyLineRenderable curve1;

    ModelContext modelContext;



    public GLRenderer(ModelContext modelContext)
    {
        this.modelContext = modelContext;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {

        fovy = 45.0f;
        //aspect @onSurfaceChanged
        zNear = MODEL_FRONT_Z;
        zFar = MODEL_BACK_Z;

        eyeVec = new float[4];
        centerVec = new float[4];
        upVec = new float[4];
        rightVec = new float[4];

        centerVec[0] = 0.0f;
        centerVec[1] = 0.0f;
        centerVec[2] = 0.0f;
        centerVec[3] = 1.0f;

        upVec[0] = 0.0f;
        upVec[1] = 1.0f;
        upVec[2] = 0.0f;
        upVec[3] = 1.0f;


        //Compile shared shaders
        Vector3Renderable.generateShaders();
        PolyLineRenderable.generateShaders();
        ParametricSurface.generateShaders();
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
        aspect = (1.0f*width)/height;
        eyeVec[0] = 0.0f;
        eyeVec[1] = 0.0f;
        eyeVec[2] = (float)(height/(2*Math.tan(Math.toRadians(45.0/2))));
        eyeVec[3] = 1.0f;
        recalculateRightVec();
        recalculateViewMatrix();
        recalculateProjectionMatrix();
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        GLES20.glClearColor(RenderingPreferences.BackgroundColor[0], RenderingPreferences.BackgroundColor[1], RenderingPreferences.BackgroundColor[2], RenderingPreferences.BackgroundColor[3]);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        if (modelContext != null)
        {
            modelContext.render(MVPMatrix);
        }
    }

    //MVP manipulation
    //event based bottom -> top
    //
    void recalculateMVPMatrix()
    {
        Matrix.multiplyMM(MVPMatrix, 0, projectionMatrix, 0, viewMatrix, 0);
    }

    void recalculateViewMatrix()
    {
        Matrix.setLookAtM(viewMatrix, 0, eyeVec[0]/eyeVec[3], eyeVec[1]/eyeVec[3], eyeVec[2]/eyeVec[3],
                centerVec[0]/centerVec[3], centerVec[1]/centerVec[3], centerVec[2]/centerVec[3],
                upVec[0]/upVec[3], upVec[1]/upVec[3], upVec[2]/upVec[3]);
        recalculateMVPMatrix();
    }

    void recalculateProjectionMatrix()
    {
        Matrix.perspectiveM(projectionMatrix, 0, fovy, aspect, zNear, zFar);
        recalculateMVPMatrix();
    }

    void recalculateRightVec()
    {
        float[] diff = Vector.subtractAsNorm(eyeVec,centerVec);
        float[] rotM = new float[16];
        Matrix.setRotateM(rotM, 0, -90.0f, diff[0] / diff[3], diff[1] / diff[3], diff[2] / diff[3]);
        Matrix.multiplyMV(rightVec, 0, rotM, 0, upVec, 0);

    }

    //Navigation
    //
    public void rotate(float rx ,float ry)
    {
        rx*=ROTATION_COEFF;
        ry*=ROTATION_COEFF;
        float[] scrollVec = Vector.addAsNorm(Vector.multiplyAsNorm(rightVec, rx), Vector.multiplyAsNorm(upVec, ry));
        float[] diff = Vector.subtractAsNorm(eyeVec,centerVec);
        float[] rotM = new float[16];
        Matrix.setRotateM(rotM, 0, 90.0f, diff[0]/diff[3], diff[1]/diff[3], diff[2]/diff[3]);
        float[] rotVec = new float[4];
        Matrix.multiplyMV(rotVec, 0, rotM,0 , scrollVec, 0);
        Matrix.setRotateM(rotM, 0, Vector.lengthAsNorm(scrollVec), rotVec[0]/rotVec[3], rotVec[1]/rotVec[3], rotVec[2]/rotVec[3]);
        //TODO: try inplace
        float[] newEyeVec = new float[4];
        float[] newUpVec = new float[4];
        Matrix.multiplyMV(newEyeVec, 0, rotM,0 , eyeVec, 0);
        Matrix.multiplyMV(newUpVec, 0, rotM,0 , upVec, 0);
        eyeVec = newEyeVec;
        upVec = newUpVec;
        recalculateRightVec();
        recalculateViewMatrix();
    }

    public void move(float dx,float dy)
    {
        float[] moveVec = Vector.addAsNorm(Vector.multiplyAsNorm(rightVec,dx),Vector.multiplyAsNorm(upVec,dy));
        eyeVec = Vector.addAsNorm(eyeVec, moveVec);
        centerVec = Vector.addAsNorm(centerVec,moveVec);
        recalculateViewMatrix();
    }

    public void zoom(float ratio)
    {
        float[] diff = Vector.subtractAsNorm(eyeVec, centerVec);
        diff = Vector.multiplyAsNorm(diff, 1 / ratio);
        eyeVec = Vector.addAsNorm(centerVec,diff);
        recalculateViewMatrix();
    }


    //Utility
    public static int loadShader(int type, String shaderCode){
        int shader = GLES20.glCreateShader(type);
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);
        return shader;
    }

    public void setModelContext(ModelContext modelContext)
    {
        this.modelContext = modelContext;
    }


    public float[] getMVP()
    {
        return MVPMatrix;
    }



}
