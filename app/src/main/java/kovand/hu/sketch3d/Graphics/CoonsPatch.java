package kovand.hu.sketch3d.Graphics;

import kovand.hu.sketch3d.Vector.Vector;
import android.opengl.GLES20;
import android.util.Log;

import java.io.IOException;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;


public class CoonsPatch extends ParametricSurface {

    ParametricCurve c0;
    ParametricCurve c1;
    ParametricCurve d0;
    ParametricCurve d1;

    public CoonsPatch(ParametricCurve c0,ParametricCurve c1, ParametricCurve d0, ParametricCurve d1){
        this.c0 = c0;
        this.c1 = c1;
        this.d0 = d0;
        this.d1 = d1;
    }

    @Override
    public Vector evaluate(float s, float t) {

        //Lc interpolation: (1-v)c0(u)+v*c1(u)
        Vector c0s = c0.evaluate(s);
        Vector c1s = c1.evaluate(s);
        Vector Lc = Vector.add(Vector.multiply(c0s, 1 - t), Vector.multiply(c1s, t));

        //Ld interpolation
        Vector d0t = d0.evaluate(t);
        Vector d1t = d1.evaluate(t);
        Vector Ld = Vector.add(Vector.multiply(d0t, 1 - s), Vector.multiply(d1t, s));

        //Corner interpolation
        Vector c00 = c0.evaluate(0);
        Vector c01 = c0.evaluate(1);
        Vector c10 = c1.evaluate(0);
        Vector c11 = c1.evaluate(1);
        Vector B0 = Vector.multiply(c00, (1 - s) * (1 - t));
        Vector B1 = Vector.multiply(c01, s * (1 - t));
        Vector B2 = Vector.multiply(c10,(1-s)*t);
        Vector B3 = Vector.multiply(c11,s*t);
        Vector B = Vector.add(Vector.add(B0, B1), Vector.add(B2, B3));

        Vector Lc_plus_Ld = Vector.add(Lc, Ld);
        Vector result = Vector.add(Lc_plus_Ld,Vector.multiply(B,-1));
        return result;
    }

    private void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        generateRenderingData();
    }
}
