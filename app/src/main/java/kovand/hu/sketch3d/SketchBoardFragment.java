package kovand.hu.sketch3d;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.AvoidXfermode;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadPoolExecutor;

import kovand.hu.sketch3d.Graphics.GLRenderer;
import kovand.hu.sketch3d.Graphics.Vector3Renderable;
import kovand.hu.sketch3d.Model.ModelContext;
import kovand.hu.sketch3d.Model.ModelCurve;
import kovand.hu.sketch3d.Model.ModelElement;
import kovand.hu.sketch3d.Model.ModelPoint;
import kovand.hu.sketch3d.Model.ModelSurface;
import kovand.hu.sketch3d.Model.ModelSurfacePlane;
import kovand.hu.sketch3d.Vector.Vector;
import kovand.hu.sketch3d.Vector.Vector2;
import kovand.hu.sketch3d.Vector.Vector3;

public class SketchBoardFragment extends Fragment {

    public static final String TAG = "SketchBoardFragment";



    GLSurfaceView glSurfaceView;
    GLRenderer renderer;

    ScaleGestureDetector fingerScaleGestureDetector;

    MainActivity mainActivity;

    ModelContext modelContext;

    private StrokeHandler strokeHandler;

    UUID clipboard = null;

    boolean pasteAction = false;

    boolean isRotationLocked = false;



    //Finger gestures
    private GestureDetector.SimpleOnGestureListener fingerGestureListener = new GestureDetector.SimpleOnGestureListener()
    {
        public static final String TAG = "fingerGestureListener";

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            Log.d(TAG, "Finger event: SingleTapConfirmed");
            return true;
        }

        @Override
        public void onLongPress(final MotionEvent e) {
            Log.d(TAG, "Finger event: LongPress");
        }

        @Override
        public boolean onDoubleTap(final MotionEvent e) {


            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2,final float distanceX,final float distanceY) {
            //TODO get access to the button
            if (isRotationLocked)
            {
                glSurfaceView.queueEvent(new Runnable() {
                    @Override
                    public void run() {
                        renderer.move(distanceX , -distanceY);
                    }
                });
            }
            else
            {
                glSurfaceView.queueEvent(new Runnable() {

                    @Override
                    public void run() {
                        renderer.rotate(distanceX , -distanceY);

                    }
                });

            }
            return true;
        }
    };
    private GestureDetector fingerDetector = new GestureDetector(getActivity(),fingerGestureListener);

    private GestureDetector.SimpleOnGestureListener penGestureListener = new GestureDetector.SimpleOnGestureListener()
    {

        @Override
        public boolean onSingleTapConfirmed(final MotionEvent e) {


            if (clipboard != null && pasteAction)
            {

                return true;

            }

            final Vector2 normScreenPos = new Vector2(e.getX()/glSurfaceView.getWidth()*2.0f-1.0f, 1.0f-e.getY()/glSurfaceView.getHeight()*2.0f);

            UUID id = modelContext.getElementByScreenPosition(normScreenPos,
                    glSurfaceView.getWidth()/2.0f,glSurfaceView.getHeight()/2.0f, renderer.getMVP());

            if (id != null)
            {
                final ModelElement elem = modelContext.getElement(id);
                glSurfaceView.queueEvent(new Runnable() {
                    @Override
                    public void run() {
                        if (modelContext.isSelected(elem.getId()))
                        {
                            modelContext.unSelect(elem.getId());
                        }
                        else
                        {
                            modelContext.select(elem.getId());
                        }

                    }
                });

            }


            return true;
        }


        @Override
        public boolean onDoubleTap(final MotionEvent e) {



            final Vector2 normScreenPos = new Vector2(e.getX()/glSurfaceView.getWidth()*2.0f-1.0f, 1.0f-e.getY()/glSurfaceView.getHeight()*2.0f);
            ModelSurfacePlane surf = (ModelSurfacePlane)modelContext.getElement(modelContext.getActiveSurface());
            final Vector2 address = Utility.mapToSurface(normScreenPos,surf.getPole(),surf.getX(),surf.getY(),renderer.getMVP());
            final ModelPoint modelPoint = new ModelPoint(modelContext, modelContext.getActiveSurface(), address);
            glSurfaceView.queueEvent(new Runnable() {
                @Override
                public void run() {

                    modelContext.addElement(modelPoint);

                }
            });


            return true;
        }

        @Override
        public void onLongPress(MotionEvent e) {

        }

    };
    private GestureDetector penDetector = new GestureDetector(getActivity(),penGestureListener);


    View.OnTouchListener onTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(final View v, final MotionEvent event) {


            int id = 0;
            int tool = 0;
            int pointers = event.getPointerCount();
            try{
                id = event.getPointerId(0);
                tool = event.getToolType(id);
            }
            catch (IllegalArgumentException e){
                return false;
            }

            if (tool == MotionEvent.TOOL_TYPE_STYLUS)
            {
                penDetector.onTouchEvent(event);
                strokeHandler.onTouchEvent(v, event);
            }
            else //finger
            {
                if (pointers>1)
                {
                    fingerScaleGestureDetector.onTouchEvent(event);
                }
                else
                {

                    fingerDetector.onTouchEvent(event);
                }
            }
            return true;
        }
    };


    StrokeHandler.onStrokeListener strokeListener = new StrokeHandler.onStrokeListener() {

        @Override
        public void onStrokeEnd(final List<Vector2> stroke, boolean valid) {
            if (valid)
            {
                ModelSurfacePlane surf = (ModelSurfacePlane)modelContext.getElement(modelContext.getActiveSurface());
                List<Vector2> normalizedDeviceList =  Utility.fromAndroidToNormalizedDevice(stroke,glSurfaceView.getWidth(),glSurfaceView.getHeight());
                List<Vector2> surfaceMappedList =  Utility.mapToSurface(normalizedDeviceList, surf.getPole(), surf.getX(), surf.getY(), renderer.getMVP());
                final ModelCurve curve = new ModelCurve(modelContext, modelContext.getActiveSurface(),surfaceMappedList);

                glSurfaceView.queueEvent(new Runnable() {
                    @Override
                    public void run() {
                        modelContext.addElement(curve);
                    }
                });

            }

        }

        @Override
        public boolean onStrokeBegin(Vector2 address) {
            return false;
        }

        @Override
        public boolean onStroke(List<Vector2> stroke) {
            return false;
        }
    };


    ScaleGestureDetector.OnScaleGestureListener fingerScaleGestureListener = new ScaleGestureDetector.OnScaleGestureListener() {

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            return true;
        }

        @Override
        public boolean onScale(final ScaleGestureDetector detector) {

            final float scale = detector.getScaleFactor();
            glSurfaceView.queueEvent(new Runnable() {

                @Override
                public void run() {
                    renderer.zoom(scale);
                }
            });
            return true;
        }
    };





    @Override
    public void onAttach(Activity activity) {
        Log.d(TAG,"onAttach()");
        super.onAttach(activity);
        mainActivity = ((MainActivity)activity);
        mainActivity.onSectionAttached(1);
        modelContext = mainActivity.getModelContext();


        strokeHandler = new StrokeHandler();
        strokeHandler.setOnStrokeListener(strokeListener);
    }






    public static SketchBoardFragment newInstance(){
        SketchBoardFragment fragment = new SketchBoardFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        fingerScaleGestureDetector = new ScaleGestureDetector(getActivity(),fingerScaleGestureListener);

        glSurfaceView = new GLSurfaceView(getActivity());
        renderer = new GLRenderer(modelContext);
        glSurfaceView.setEGLContextClientVersion(2);
        glSurfaceView.setRenderer(renderer);
        glSurfaceView.setOnTouchListener(onTouchListener);

        return glSurfaceView;
    }

    public void newSurface()
    {
        Toast.makeText(getActivity(),"Surf",Toast.LENGTH_SHORT).show();
        List<UUID> selecteds = modelContext.getSelectedElements();
        if (selecteds.size() == 2)
        {
            ModelPoint p1 = (ModelPoint)modelContext.getElement(selecteds.get(0));
            ModelPoint p2 = (ModelPoint)modelContext.getElement(selecteds.get(1));

            final ModelSurface surface = modelContext.generatePerpedicularSurface(p1,p2);

            glSurfaceView.queueEvent(new Runnable() {
                @Override
                public void run() {
                    modelContext.addElement(surface);
                    modelContext.setActiveSurface(surface.getId());
                }
            });
        }
    }

    public void deleteSelectedElements()
    {
        glSurfaceView.queueEvent(new Runnable() {
            @Override
            public void run() {
                modelContext.deleteElement(modelContext.getSelectedElements());
            }
        });

    }


    public void makeCoons()
    {
        modelContext.generateCoons();
    }

    public void lockRotation(boolean locked)
    {
        isRotationLocked = locked;

    }


}