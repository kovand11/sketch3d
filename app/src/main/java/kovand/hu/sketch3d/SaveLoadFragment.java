package kovand.hu.sketch3d;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.List;




interface SaveLoadDeleteListener{
    void onSave(String name);
    void onLoad(String name);
    void onDelete(String name);
}

public class SaveLoadFragment extends Fragment {

    EditText saveFileText;
    Button saveButton;
    Button browseButton;

    List<String> recentFiles;
    ListView recentList;
    ArrayAdapter<String> listAdapter;


    SaveLoadDeleteListener saveLoadDeleteListener;


    public static SaveLoadFragment newInstance(){
        SaveLoadFragment fragment = new SaveLoadFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_save_load,container,false);

        saveFileText = (EditText)rootView.findViewById(R.id.save_file_text);
        saveButton = (Button)rootView.findViewById(R.id.save_button);
        browseButton = (Button)rootView.findViewById(R.id.browse_button);
        recentList = (ListView)rootView.findViewById(R.id.recent_list);


        listAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,recentFiles);
        recentList.setAdapter(listAdapter);

        recentList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String s = String.valueOf(parent.getItemAtPosition(position));
                saveLoadDeleteListener.onLoad(s);
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = String.valueOf(saveFileText.getText());
                saveLoadDeleteListener.onSave(str);
                recentFiles = ((MainActivity)getActivity()).getFiles();
                listAdapter.notifyDataSetChanged();
            }
        });

        registerForContextMenu(recentList);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity)activity).onSectionAttached(3);

        recentFiles = ((MainActivity)activity).getFiles();
        saveLoadDeleteListener = (SaveLoadDeleteListener)activity;
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_load_list, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        String title = String.valueOf(item.getTitle());
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (title)
        {
            case "Delete":
                saveLoadDeleteListener.onDelete(listAdapter.getItem((int)info.id));
                recentFiles = ((MainActivity)getActivity()).getFiles();
                listAdapter.notifyDataSetChanged();
                break;

            case "Overwrite":
                saveLoadDeleteListener.onSave(listAdapter.getItem((int) info.id));
                break;
        }

        return true;
    }
}
